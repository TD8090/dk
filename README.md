![DiscKicker banner](http://i.imgur.com/YGcFsY4.png)

*   Thank you for visiting DiscKicker Pre-alpha!
*   DiscKicker intends to be a disc golf social network that helps its members set up new golf rounds and other interesting challenges in and around Charlotte, NC.


# What can I expect from the DiscKicker community?
##   Safety and Etiquette Guidelines
There is no restriction on joining, so beware of the people behind newer or low reputation accounts.
##   Reputation
Your DK profile stats will reflect on your credibility and reputation as a DK citizen, just like on ebay for example.  
Making a positive impact on the cards you join will help your stats to rise, which makes you a more appealing citizen when you want to join cards later.
## Honor System
You are bound by your word to provide your own transportation and show up on time at the time and place agreed upon.
If you fail, you will lose votes and your reputation score will suffer.



# What's currently in the app?
## Round-Card Hosting & Joining System
#### As a host:
1. Use the card creator to create a card based on a specific challenge/location and time.  If you want to tweak the rules a bit, make sure to include it in the description.
2. Players will send Offers to Join (OTJ) when they are interested in the challenge specified.
3. Consider which players to invite, and send out invitations to join (ITJ) the card.
4. Card Capacity - You can send out more than 3 invitations, but all excess invitations sent will vanish when the max players has been reached.  At this time when the card capacity is full and max players is reached, Guests may still OTJ (express interest), but new invitations may not be sent.
5. In the event a player drops or is removed from the card, new invitations may once again be sent to players appearing on the hosts OTJ list for that card.
6. As the round host, you'll be judged by how good of a host you were, so it's important to assume the role of chief coordinator for the card guests interested in the challenge.  If they have questions related to the round that go unanswered, they will probably drop from the card.
#### As a guest:
1. Browse cards that others have posted, and when you find a challenge that suits your tastes, send the host an offer to join the card with the push of a button.
2. Once a host invites you, you must accept the invitation to join.  This ensures that you are still on-board and haven't become doublebooked.
3. Once you join a card, your stats are subject to change depending on the outcome.



# What's next?
## DK points and ERS
DiscKicker made to track winners and losers and no-shows of rounds.  This is to be accomplished
    by implementation of an "Effortless Reporting System"- a quick survey that appears
    after rounds are played in order to track the outcomes and adjust player stats accordingly.
## Rep points, Reports
Players will be given the ability to rate and report other players
## DK point wagers
Players will be given the ability to wager DK points on rounds of golf



# What's later?
## In-app challenges
Easy/Difficult challenge achievements that require 2 or more DK citizen witnesses to count.
    Encourages 3 and 4 man cards.
## Share box
Give hosts the ability to share additional information and content with interested players.
    This can be pictures, links, or formatted text.  It helps broaden the options for club event
    handlers to operate and advertise from within DiscKicker.
## In-app purchases
Support the app and buy exclusive disc golf gear and merchandise right from within DiscKicker!
