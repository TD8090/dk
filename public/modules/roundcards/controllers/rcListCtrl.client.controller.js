'use strict';
var rcMod = angular.module('roundcards');

rcMod.controller('rcListCtrl', ['$scope', '$stateParams', '$location', 'Authentication', 'Roundcards',
    function($scope, $stateParams, $location, Authentication, Roundcards ) {
        $scope.authentication = Authentication;


        // Find a list of Roundcards
        $scope.find = function() {
            $scope.roundcards = Roundcards.query();
        };

        // Find existing Roundcard
        $scope.findOne = function() {
            $scope.roundcard = Roundcards.get({
                roundcardId: $stateParams.roundcardId
            });
        };

        /* test location to create stylings with ng-class */
        $scope.getLocClass = function(loc){
            var locPass = loc;
            var locMatchObj = {
                'Eastway Park(S)'       :'rc-loc-eastway-s',
                'Eastway Park(L)'       :'rc-loc-eastway-l',
                'Kilborne TPC(S)'       :'rc-loc-kilborne-s',
                'Kilborne TPC(L)'       :'rc-loc-kilborne-l',
                'Reedy Creek(S)'        :'rc-loc-reedy-s',
                'Reedy Creek(L)'        :'rc-loc-reedy-l',
                'Sugaw Creek Park'      :'rc-loc-sugaw',
                'Hornets Nest(S)'       :'rc-loc-horn-s',
                'Hornets Nest(L)'       :'rc-loc-horn-l',
                'Nevin Nightmare(S)'    :'rc-loc-nevin-s',
                'Nevin Nightmare(L)'    :'rc-loc-nevin-l',
                'Goat Island DGC'       :'rc-loc-goatisland',
                'R.L. Smith DGC'        :'rc-loc-rlsmith',
                'Rennaissance Grey'     :'rc-loc-renn-grey',
                'Rennaissance Gold'     :'rc-loc-renn-gold',
                'Elon Eager Beaver'     :'rc-loc-elon-eager',
                'Elon Angry Beaver'     :'rc-loc-elon-angry',
                'Winget - Ruins'        :'rc-loc-winget',
                'Crooked Creek'         :'rc-loc-crooked',
                'Dry Creek DGC'         :'rc-loc-drycreek',
                'Idlewild Scrapyard'    :'rc-loc-idlewild',
                'Squirrel Lake Park'    :'rc-loc-squirrel',
                'Bailey Road Park'      :'rc-loc-bailey',
                'Brackett\'s Bluff'     :'rc-loc-brackett',
                'Bradford Park'         :'rc-loc-bradford',
                'Davidson College'      :'rc-loc-davidson',
                'Bradley Center'        :'rc-loc-bradley',
                'Rankin Lake'           :'rc-loc-rankin',
                'Boyd Hill'             :'rc-loc-boyd',
                'Winthrop TPC'          :'rc-loc-winthrop'
                };
            return locMatchObj[locPass];
        };
        /* end getLocClass */
        /* test location to create triangle color with ng-class */
        $scope.getLocTriClass = function(loctri){
            var loctriPass = loctri;
            var loctriMatchObj = {
                'Eastway Park(S)'       :'loctri-eastway-s',
                'Eastway Park(L)'       :'loctri-eastway-l',
                'Kilborne TPC(S)'       :'loctri-kilborne',
                'Kilborne TPC(L)'       :'loctri-kilborne',
                'Reedy Creek(S)'        :'loctri-reedy-s',
                'Reedy Creek(L)'        :'loctri-reedy-l',
                'Sugaw Creek Park'      :'loctri-sugaw',
                'Hornets Nest(S)'       :'loctri-horn-s',
                'Hornets Nest(L)'       :'loctri-horn-l',
                'Nevin Nightmare(S)'    :'loctri-nevin-s',
                'Nevin Nightmare(L)'    :'loctri-nevin-l',
                'Goat Island DGC'       :'loctri-goatisland',
                'R.L. Smith DGC'        :'loctri-rlsmith',
                'Rennaissance Grey'     :'loctri-renn-grey',
                'Rennaissance Gold'     :'loctri-renn-gold',
                'Elon Eager Beaver'     :'loctri-elon-eager',
                'Elon Angry Beaver'     :'loctri-elon-angry',
                'Winget - Ruins'        :'loctri-winget',
                'Crooked Creek'         :'loctri-crooked',
                'Dry Creek DGC'         :'loctri-drycreek',
                'Idlewild Scrapyard'    :'loctri-idlewild',
                'Squirrel Lake Park'    :'loctri-squirrel',
                'Bailey Road Park'      :'loctri-bailey',
                'Brackett\'s Bluff'     :'loctri-brackett',
                'Bradford Park'         :'loctri-bradford',
                'Davidson College'      :'loctri-davidson',
                'Bradley Center'        :'loctri-bradley',
                'Rankin Lake'           :'loctri-rankin',
                'Boyd Hill'             :'loctri-boyd',
                'Winthrop TPC'          :'loctri-winthrop'
            };
            return loctriMatchObj[loctriPass];
        };
        /* end getLocTriClass */

    }
]);