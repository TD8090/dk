'use strict';
var rcMod = angular.module('roundcards');

rcMod.controller('rcCreateCtrl', ['$scope', '$stateParams', '$location', 'Authentication', 'Roundcards',
    function ($scope, $stateParams, $location, Authentication, Roundcards) {
        $scope.authentication = Authentication;

        /*BEGIN: -D -A -T -E -P -I -C -K -E -R VARS*/
        $scope.oneAtATime = true;
        $scope.setlocation = {coursefromset: ''};
        $scope.$watch('setlocation.coursefromset', function () {
        });
        $scope.status = {
            isFirstOpen: true,
            isFirstDisabled: false
        };
        /*END: -D -A -T -E -P -I -C -K -E -R VARS*/

        /*GOOGLE MAPS*/
        $scope.map = {
            center: {
                latitude: 35.2,
                longitude: -80.898
            },
            draggable: false,
            zoom: 9
        };
        /*GOOGLE MAPS*/


        // Create new Roundcard
        $scope.createRC = function () {
            // Create new Roundcard object
            var roundcard = new Roundcards({
                setmaxplayers: this.setmaxplayers,
                setlocation: this.setlocation.coursefromset,
                setteetime: this.setteetime,
                setpreferredskill: this.setpreferredskill,
                setpreferredattitude: this.setpreferredattitude,
                roundheadline: this.roundheadline,
                rounddescription: this.rounddescription,
                user: this.user,
                createdtime: this.createdtime
            });
            // Redirect after save
            roundcard.$save(function (response) {
                $location.path('roundcards/' + response._id);
                // Clear form fields

                $scope.setmaxplayers = '';
                $scope.setlocation = '';
                $scope.setteetime = '';
                $scope.setpreferredskill = '';
                $scope.setattitude = '';
                $scope.roundheadline = '';
                $scope.rounddescription = '';
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        // /END/ Create new Roundcard
        // Find a list of Roundcards
        $scope.find = function() {
            $scope.roundcards = Roundcards.query();
        };

    }
]);