'use strict';
var rcMod = angular.module('roundcards');

rcMod.controller('rcEditCtrl', ['$scope', '$stateParams', '$location', 'Authentication', 'Roundcards',
    function($scope, $stateParams, $location, Authentication, Roundcards ) {
        $scope.authentication = Authentication;


        // Update existing Roundcard
        $scope.update = function() {
            var roundcard = $scope.roundcard ;

            roundcard.$update(function() {
                $location.path('roundcards/' + roundcard._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };




    }
]);