'use strict';

//Roundcards service used to communicate Roundcards REST endpoints
angular.module('roundcards').factory('Roundcards', ['$resource',
	function($resource) {
		return $resource('roundcards/:roundcardId', { roundcardId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);