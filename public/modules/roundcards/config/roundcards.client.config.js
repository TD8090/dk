'use strict';

// Configuring the Articles module
angular.module('roundcards').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Roundcards', 'rcdropdown', 'dropdown', '/roundcards(/create)?');
		Menus.addSubMenuItem('topbar', 'rcdropdown', 'List Roundcards', 'roundcards');
		Menus.addSubMenuItem('topbar', 'rcdropdown', 'New Roundcard', 'roundcards/create');
		Menus.addMenuItem('topbar', 'New', 'roundcards/create');
		Menus.addMenuItem('topbar', 'List All', 'roundcards');
	}
]);

    /*
addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
    */
