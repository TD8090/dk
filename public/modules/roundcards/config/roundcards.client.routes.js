'use strict';
angular.module('roundcards').config(['$stateProvider',      //Setting up route
    function($stateProvider) {
		// Roundcards state routing
		$stateProvider.
		state('listRoundcards', {
			url: '/roundcards',
			templateUrl: 'modules/roundcards/views/list-roundcards.client.view.html'
		}).
		state('createRoundcard', {
			url: '/roundcards/create',
			templateUrl: 'modules/roundcards/views/create-roundcard.client.view.html'
		}).
		state('viewRoundcard', {
			url: '/roundcards/:roundcardId',
			templateUrl: 'modules/roundcards/views/view-roundcard.client.view.html'
		}).
		state('editRoundcard', {
			url: '/roundcards/:roundcardId/edit',
			templateUrl: 'modules/roundcards/views/edit-roundcard.client.view.html'
		});
	}
]);