'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication',
	function($scope, Authentication) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
        $scope.bannerimg = '/modules/core/img/brand/top_logo.png';
        $scope.welcome = '/modules/core/img/brand/welcome.png';

    }
]);