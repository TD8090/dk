'use strict';
//**
// * To add third-party modules use the public/config.js file where we added an array property called applicationModuleVendorDependencies.
// * When you add a new third-party module you should add it to this array so the main module can load it as a depenedency.*/
// Init the application configuration module for AngularJS application
var ApplicationConfiguration = function () {
    // Init module configuration options
    var applicationModuleName = 'dkapp';
    var applicationModuleVendorDependencies = [
        'ngResource',
        'ngCookies',
        'ngAnimate',
        'ngTouch',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'ui.utils',
        'google-maps'
      ];
    // Add a new vertical module
    var registerModule = function (moduleName, dependencies) {
      // Create angular module
      angular.module(moduleName, dependencies || []);
      // Add the module to the AngularJS configuration file
      angular.module(applicationModuleName).requires.push(moduleName);
    };
    return {
      applicationModuleName: applicationModuleName,
      applicationModuleVendorDependencies: applicationModuleVendorDependencies,
      registerModule: registerModule
    };
  }();'use strict';
//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);
// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config([
  '$locationProvider',
  function ($locationProvider) {
    $locationProvider.hashPrefix('!');
  }
]);
//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash === '#_=_')
    window.location.hash = '#!';
  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');'use strict';
// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('roundcards');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');'use strict';
// Setting up route
angular.module('core').config([
  '$stateProvider',
  '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise('/');
    // Home state routing
    $stateProvider.state('home', {
      url: '/',
      templateUrl: 'modules/core/views/home.client.view.html'
    });
  }
]);'use strict';
angular.module('core').controller('HeaderController', [
  '$scope',
  'Authentication',
  'Menus',
  function ($scope, Authentication, Menus) {
    $scope.authentication = Authentication;
    $scope.isCollapsed = false;
    $scope.menu = Menus.getMenu('topbar');
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };
    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
  }
]);'use strict';
angular.module('core').controller('HomeController', [
  '$scope',
  'Authentication',
  function ($scope, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    $scope.bannerimg = '/modules/core/img/brand/top_logo.png';
    $scope.welcome = '/modules/core/img/brand/welcome.png';
  }
]);'use strict';
//Menu service used for managing  menus
angular.module('core').service('Menus', [function () {
    // Define a set of default roles
    this.defaultRoles = ['*'];
    // Define the menus object
    this.menus = {};
    // A private function for rendering decision 
    var shouldRender = function (user) {
      if (user) {
        if (!!~this.roles.indexOf('*')) {
          return true;
        } else {
          for (var userRoleIndex in user.roles) {
            for (var roleIndex in this.roles) {
              if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
                return true;
              }
            }
          }
        }
      } else {
        return this.isPublic;
      }
      return false;
    };
    // Validate menu existance
    this.validateMenuExistance = function (menuId) {
      if (menuId && menuId.length) {
        if (this.menus[menuId]) {
          return true;
        } else {
          throw new Error('Menu does not exists');
        }
      } else {
        throw new Error('MenuId was not provided');
      }
      return false;
    };
    // Get the menu object by menu id
    this.getMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      return this.menus[menuId];
    };
    // Add new menu object by menu id
    this.addMenu = function (menuId, isPublic, roles) {
      // Create the new menu
      this.menus[menuId] = {
        isPublic: isPublic || false,
        roles: roles || this.defaultRoles,
        items: [],
        shouldRender: shouldRender
      };
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      delete this.menus[menuId];
    };
    // Add menu item object
    this.addMenuItem = function (menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Push new menu item
      this.menus[menuId].items.push({
        title: menuItemTitle,
        link: menuItemURL,
        menuItemType: menuItemType || 'item',
        menuItemClass: menuItemType,
        uiRoute: menuItemUIRoute || '/' + menuItemURL,
        isPublic: isPublic === null || typeof isPublic === 'undefined' ? this.menus[menuId].isPublic : isPublic,
        roles: roles === null || typeof roles === 'undefined' ? this.menus[menuId].roles : roles,
        position: position || 0,
        items: [],
        shouldRender: shouldRender
      });
      // Return the menu object
      return this.menus[menuId];
    };
    // Add submenu item object
    this.addSubMenuItem = function (menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
          // Push new submenu item
          this.menus[menuId].items[itemIndex].items.push({
            title: menuItemTitle,
            link: menuItemURL,
            uiRoute: menuItemUIRoute || '/' + menuItemURL,
            isPublic: isPublic === null || typeof isPublic === 'undefined' ? this.menus[menuId].items[itemIndex].isPublic : isPublic,
            roles: roles === null || typeof roles === 'undefined' ? this.menus[menuId].items[itemIndex].roles : roles,
            position: position || 0,
            shouldRender: shouldRender
          });
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenuItem = function (menuId, menuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
          this.menus[menuId].items.splice(itemIndex, 1);
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeSubMenuItem = function (menuId, submenuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
          if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
            this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
          }
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    //Adding the topbar menu
    this.addMenu('topbar');
  }]);'use strict';
// Configuring the Articles module
angular.module('roundcards').run([
  'Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', 'Roundcards', 'rcdropdown', 'dropdown', '/roundcards(/create)?');
    Menus.addSubMenuItem('topbar', 'rcdropdown', 'List Roundcards', 'roundcards');
    Menus.addSubMenuItem('topbar', 'rcdropdown', 'New Roundcard', 'roundcards/create');
    Menus.addMenuItem('topbar', 'New', 'roundcards/create');
    Menus.addMenuItem('topbar', 'List All', 'roundcards');
  }
]);  /*
addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
    */'use strict';
angular.module('roundcards').config([
  '$stateProvider',
  function ($stateProvider) {
    // Roundcards state routing
    $stateProvider.state('listRoundcards', {
      url: '/roundcards',
      templateUrl: 'modules/roundcards/views/list-roundcards.client.view.html'
    }).state('createRoundcard', {
      url: '/roundcards/create',
      templateUrl: 'modules/roundcards/views/create-roundcard.client.view.html'
    }).state('viewRoundcard', {
      url: '/roundcards/:roundcardId',
      templateUrl: 'modules/roundcards/views/view-roundcard.client.view.html'
    }).state('editRoundcard', {
      url: '/roundcards/:roundcardId/edit',
      templateUrl: 'modules/roundcards/views/edit-roundcard.client.view.html'
    });
  }
]);'use strict';
var rcMod = angular.module('roundcards');
rcMod.controller('rcCreateCtrl', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Roundcards',
  function ($scope, $stateParams, $location, Authentication, Roundcards) {
    $scope.authentication = Authentication;
    /*BEGIN: -D -A -T -E -P -I -C -K -E -R VARS*/
    $scope.oneAtATime = true;
    $scope.setlocation = { coursefromset: '' };
    $scope.$watch('setlocation.coursefromset', function () {
    });
    $scope.status = {
      isFirstOpen: true,
      isFirstDisabled: false
    };
    /*END: -D -A -T -E -P -I -C -K -E -R VARS*/
    /*GOOGLE MAPS*/
    $scope.map = {
      center: {
        latitude: 35.2,
        longitude: -80.898
      },
      draggable: false,
      zoom: 9
    };
    /*GOOGLE MAPS*/
    // Create new Roundcard
    $scope.createRC = function () {
      // Create new Roundcard object
      var roundcard = new Roundcards({
          setmaxplayers: this.setmaxplayers,
          setlocation: this.setlocation.coursefromset,
          setteetime: this.setteetime,
          setpreferredskill: this.setpreferredskill,
          setpreferredattitude: this.setpreferredattitude,
          roundheadline: this.roundheadline,
          rounddescription: this.rounddescription,
          user: this.user,
          createdtime: this.createdtime
        });
      // Redirect after save
      roundcard.$save(function (response) {
        $location.path('roundcards/' + response._id);
        // Clear form fields
        $scope.setmaxplayers = '';
        $scope.setlocation = '';
        $scope.setteetime = '';
        $scope.setpreferredskill = '';
        $scope.setattitude = '';
        $scope.roundheadline = '';
        $scope.rounddescription = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // /END/ Create new Roundcard
    // Find a list of Roundcards
    $scope.find = function () {
      $scope.roundcards = Roundcards.query();
    };
  }
]);'use strict';
var rcMod = angular.module('roundcards');
rcMod.controller('rcEditCtrl', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Roundcards',
  function ($scope, $stateParams, $location, Authentication, Roundcards) {
    $scope.authentication = Authentication;
    // Update existing Roundcard
    $scope.update = function () {
      var roundcard = $scope.roundcard;
      roundcard.$update(function () {
        $location.path('roundcards/' + roundcard._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
  }
]);'use strict';
var rcMod = angular.module('roundcards');
rcMod.controller('rcListCtrl', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Roundcards',
  function ($scope, $stateParams, $location, Authentication, Roundcards) {
    $scope.authentication = Authentication;
    // Find a list of Roundcards
    $scope.find = function () {
      $scope.roundcards = Roundcards.query();
    };
    // Find existing Roundcard
    $scope.findOne = function () {
      $scope.roundcard = Roundcards.get({ roundcardId: $stateParams.roundcardId });
    };
    /* test location to create stylings with ng-class */
    $scope.getLocClass = function (loc) {
      var locPass = loc;
      var locMatchObj = {
          'Eastway Park(S)': 'rc-loc-eastway-s',
          'Eastway Park(L)': 'rc-loc-eastway-l',
          'Kilborne TPC(S)': 'rc-loc-kilborne-s',
          'Kilborne TPC(L)': 'rc-loc-kilborne-l',
          'Reedy Creek(S)': 'rc-loc-reedy-s',
          'Reedy Creek(L)': 'rc-loc-reedy-l',
          'Sugaw Creek Park': 'rc-loc-sugaw',
          'Hornets Nest(S)': 'rc-loc-horn-s',
          'Hornets Nest(L)': 'rc-loc-horn-l',
          'Nevin Nightmare(S)': 'rc-loc-nevin-s',
          'Nevin Nightmare(L)': 'rc-loc-nevin-l',
          'Goat Island DGC': 'rc-loc-goatisland',
          'R.L. Smith DGC': 'rc-loc-rlsmith',
          'Rennaissance Grey': 'rc-loc-renn-grey',
          'Rennaissance Gold': 'rc-loc-renn-gold',
          'Elon Eager Beaver': 'rc-loc-elon-eager',
          'Elon Angry Beaver': 'rc-loc-elon-angry',
          'Winget - Ruins': 'rc-loc-winget',
          'Crooked Creek': 'rc-loc-crooked',
          'Dry Creek DGC': 'rc-loc-drycreek',
          'Idlewild Scrapyard': 'rc-loc-idlewild',
          'Squirrel Lake Park': 'rc-loc-squirrel',
          'Bailey Road Park': 'rc-loc-bailey',
          'Brackett\'s Bluff': 'rc-loc-brackett',
          'Bradford Park': 'rc-loc-bradford',
          'Davidson College': 'rc-loc-davidson',
          'Bradley Center': 'rc-loc-bradley',
          'Rankin Lake': 'rc-loc-rankin',
          'Boyd Hill': 'rc-loc-boyd',
          'Winthrop TPC': 'rc-loc-winthrop'
        };
      return locMatchObj[locPass];
    };
    /* end getLocClass */
    /* test location to create triangle color with ng-class */
    $scope.getLocTriClass = function (loctri) {
      var loctriPass = loctri;
      var loctriMatchObj = {
          'Eastway Park(S)': 'loctri-eastway-s',
          'Eastway Park(L)': 'loctri-eastway-l',
          'Kilborne TPC(S)': 'loctri-kilborne',
          'Kilborne TPC(L)': 'loctri-kilborne',
          'Reedy Creek(S)': 'loctri-reedy-s',
          'Reedy Creek(L)': 'loctri-reedy-l',
          'Sugaw Creek Park': 'loctri-sugaw',
          'Hornets Nest(S)': 'loctri-horn-s',
          'Hornets Nest(L)': 'loctri-horn-l',
          'Nevin Nightmare(S)': 'loctri-nevin-s',
          'Nevin Nightmare(L)': 'loctri-nevin-l',
          'Goat Island DGC': 'loctri-goatisland',
          'R.L. Smith DGC': 'loctri-rlsmith',
          'Rennaissance Grey': 'loctri-renn-grey',
          'Rennaissance Gold': 'loctri-renn-gold',
          'Elon Eager Beaver': 'loctri-elon-eager',
          'Elon Angry Beaver': 'loctri-elon-angry',
          'Winget - Ruins': 'loctri-winget',
          'Crooked Creek': 'loctri-crooked',
          'Dry Creek DGC': 'loctri-drycreek',
          'Idlewild Scrapyard': 'loctri-idlewild',
          'Squirrel Lake Park': 'loctri-squirrel',
          'Bailey Road Park': 'loctri-bailey',
          'Brackett\'s Bluff': 'loctri-brackett',
          'Bradford Park': 'loctri-bradford',
          'Davidson College': 'loctri-davidson',
          'Bradley Center': 'loctri-bradley',
          'Rankin Lake': 'loctri-rankin',
          'Boyd Hill': 'loctri-boyd',
          'Winthrop TPC': 'loctri-winthrop'
        };
      return loctriMatchObj[loctriPass];
    };  /* end getLocTriClass */
  }
]);'use strict';
var rcMod = angular.module('roundcards');
rcMod.controller('rcViewCtrl', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Roundcards',
  function ($scope, $stateParams, $location, Authentication, Roundcards) {
    $scope.authentication = Authentication;
    // Find existing Roundcard
    $scope.findOne = function () {
      $scope.roundcard = Roundcards.get({ roundcardId: $stateParams.roundcardId });
    };
    // Remove existing Roundcard
    $scope.remove = function (roundcard) {
      if (roundcard) {
        roundcard.$remove();
        for (var i in $scope.roundcards) {
          if ($scope.roundcards[i] === roundcard) {
            $scope.roundcards.splice(i, 1);
          }
        }
      } else {
        $scope.roundcard.$remove(function () {
          $location.path('roundcards');
        });
      }
    };
    /* test location to create stylings with ng-class */
    $scope.getLocClass = function (loc) {
      var locPass = loc;
      var locMatchObj = {
          'Eastway Park(S)': 'rc-loc-eastway-s',
          'Eastway Park(L)': 'rc-loc-eastway-l',
          'Kilborne TPC(S)': 'rc-loc-kilborne-s',
          'Kilborne TPC(L)': 'rc-loc-kilborne-l',
          'Reedy Creek(S)': 'rc-loc-reedy-s',
          'Reedy Creek(L)': 'rc-loc-reedy-l',
          'Sugaw Creek Park': 'rc-loc-sugaw',
          'Hornets Nest(S)': 'rc-loc-horn-s',
          'Hornets Nest(L)': 'rc-loc-horn-l',
          'Nevin Nightmare(S)': 'rc-loc-nevin-s',
          'Nevin Nightmare(L)': 'rc-loc-nevin-l',
          'Goat Island DGC': 'rc-loc-goatisland',
          'R.L. Smith DGC': 'rc-loc-rlsmith',
          'Rennaissance Grey': 'rc-loc-renn-grey',
          'Rennaissance Gold': 'rc-loc-renn-gold',
          'Elon Eager Beaver': 'rc-loc-elon-eager',
          'Elon Angry Beaver': 'rc-loc-elon-angry',
          'Winget - Ruins': 'rc-loc-winget',
          'Crooked Creek': 'rc-loc-crooked',
          'Dry Creek DGC': 'rc-loc-drycreek',
          'Idlewild Scrapyard': 'rc-loc-idlewild',
          'Squirrel Lake Park': 'rc-loc-squirrel',
          'Bailey Road Park': 'rc-loc-bailey',
          'Brackett\'s Bluff': 'rc-loc-brackett',
          'Bradford Park': 'rc-loc-bradford',
          'Davidson College': 'rc-loc-davidson',
          'Bradley Center': 'rc-loc-bradley',
          'Rankin Lake': 'rc-loc-rankin',
          'Boyd Hill': 'rc-loc-boyd',
          'Winthrop TPC': 'rc-loc-winthrop'
        };
      return locMatchObj[locPass];
    };
    /* end getLocClass */
    /* test location to create triangle color with ng-class */
    $scope.getLocTriClass = function (loctri) {
      var loctriPass = loctri;
      var loctriMatchObj = {
          'Eastway Park(S)': 'loctri-eastway-s',
          'Eastway Park(L)': 'loctri-eastway-l',
          'Kilborne TPC(S)': 'loctri-kilborne',
          'Kilborne TPC(L)': 'loctri-kilborne',
          'Reedy Creek(S)': 'loctri-reedy-s',
          'Reedy Creek(L)': 'loctri-reedy-l',
          'Sugaw Creek Park': 'loctri-sugaw',
          'Hornets Nest(S)': 'loctri-horn-s',
          'Hornets Nest(L)': 'loctri-horn-l',
          'Nevin Nightmare(S)': 'loctri-nevin-s',
          'Nevin Nightmare(L)': 'loctri-nevin-l',
          'Goat Island DGC': 'loctri-goatisland',
          'R.L. Smith DGC': 'loctri-rlsmith',
          'Rennaissance Grey': 'loctri-renn-grey',
          'Rennaissance Gold': 'loctri-renn-gold',
          'Elon Eager Beaver': 'loctri-elon-eager',
          'Elon Angry Beaver': 'loctri-elon-angry',
          'Winget - Ruins': 'loctri-winget',
          'Crooked Creek': 'loctri-crooked',
          'Dry Creek DGC': 'loctri-drycreek',
          'Idlewild Scrapyard': 'loctri-idlewild',
          'Squirrel Lake Park': 'loctri-squirrel',
          'Bailey Road Park': 'loctri-bailey',
          'Brackett\'s Bluff': 'loctri-brackett',
          'Bradford Park': 'loctri-bradford',
          'Davidson College': 'loctri-davidson',
          'Bradley Center': 'loctri-bradley',
          'Rankin Lake': 'loctri-rankin',
          'Boyd Hill': 'loctri-boyd',
          'Winthrop TPC': 'loctri-winthrop'
        };
      return loctriMatchObj[loctriPass];
    };  /* end getLocTriClass */
  }
]);'use strict';
//Roundcards service used to communicate Roundcards REST endpoints
angular.module('roundcards').factory('Roundcards', [
  '$resource',
  function ($resource) {
    return $resource('roundcards/:roundcardId', { roundcardId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Config HTTP Error Handling
angular.module('users').config([
  '$httpProvider',
  function ($httpProvider) {
    // Set the httpProvider "not authorized" interceptor
    $httpProvider.interceptors.push([
      '$q',
      '$location',
      'Authentication',
      function ($q, $location, Authentication) {
        return {
          responseError: function (rejection) {
            switch (rejection.status) {
            case 401:
              // Deauthenticate the global user
              Authentication.user = null;
              // Redirect to signin page
              $location.path('signin');
              break;
            case 403:
              // Add unauthorized behaviour 
              break;
            }
            return $q.reject(rejection);
          }
        };
      }
    ]);
  }
]);'use strict';
// Setting up route
angular.module('users').config([
  '$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider.state('profile', {
      url: '/settings/profile',
      templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
    }).state('password', {
      url: '/settings/password',
      templateUrl: 'modules/users/views/settings/change-password.client.view.html'
    }).state('accounts', {
      url: '/settings/accounts',
      templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
    }).state('signup', {
      url: '/signup',
      templateUrl: 'modules/users/views/authentication/signup.client.view.html'
    }).state('signin', {
      url: '/signin',
      templateUrl: 'modules/users/views/authentication/signin.client.view.html'
    }).state('forgot', {
      url: '/password/forgot',
      templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
    }).state('reset-invlaid', {
      url: '/password/reset/invalid',
      templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
    }).state('reset-success', {
      url: '/password/reset/success',
      templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
    }).state('reset', {
      url: '/password/reset/:token',
      templateUrl: 'modules/users/views/password/reset-password.client.view.html'
    });
  }
]);'use strict';
angular.module('users').controller('AuthenticationController', [
  '$scope',
  '$http',
  '$location',
  'Authentication',
  function ($scope, $http, $location, Authentication) {
    $scope.authentication = Authentication;
    // If user is signed in then redirect back home
    if ($scope.authentication.user)
      $location.path('/');
    $scope.signup = function () {
      $http.post('/auth/signup', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
        // And redirect to the index page
        $location.path('/');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
    $scope.signin = function () {
      $http.post('/auth/signin', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
        // And redirect to the index page
        $location.path('/');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
angular.module('users').controller('PasswordController', [
  '$scope',
  '$stateParams',
  '$http',
  '$location',
  'Authentication',
  function ($scope, $stateParams, $http, $location, Authentication) {
    $scope.authentication = Authentication;
    //If user is signed in then redirect back home
    if ($scope.authentication.user)
      $location.path('/');
    // Submit forgotten password account id
    $scope.askForPasswordReset = function () {
      $scope.success = $scope.error = null;
      $http.post('/auth/forgot', $scope.credentials).success(function (response) {
        // Show user success message and clear form
        $scope.credentials = null;
        $scope.success = response.message;
      }).error(function (response) {
        // Show user error message and clear form
        $scope.credentials = null;
        $scope.error = response.message;
      });
    };
    // Change user password
    $scope.resetUserPassword = function () {
      $scope.success = $scope.error = null;
      $http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.passwordDetails = null;
        // Attach user profile
        Authentication.user = response;
        // And redirect to the index page
        $location.path('/password/reset/success');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
angular.module('users').controller('SettingsController', [
  '$scope',
  '$http',
  '$location',
  'Users',
  'Authentication',
  function ($scope, $http, $location, Users, Authentication) {
    $scope.user = Authentication.user;
    // If user is not signed in then redirect back home
    if (!$scope.user)
      $location.path('/');
    // Check if there are additional accounts 
    $scope.hasConnectedAdditionalSocialAccounts = function (provider) {
      for (var i in $scope.user.additionalProvidersData) {
        return true;
      }
      return false;
    };
    // Check if provider is already in use with current user
    $scope.isConnectedSocialAccount = function (provider) {
      return $scope.user.provider === provider || $scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider];
    };
    // Remove a user social account
    $scope.removeUserSocialAccount = function (provider) {
      $scope.success = $scope.error = null;
      $http.delete('/users/accounts', { params: { provider: provider } }).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.user = Authentication.user = response;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
    // Update a user profile
    $scope.updateUserProfile = function (isValid) {
      if (isValid) {
        $scope.success = $scope.error = null;
        var user = new Users($scope.user);
        user.$update(function (response) {
          $scope.success = true;
          Authentication.user = response;
        }, function (response) {
          $scope.error = response.data.message;
        });
      } else {
        $scope.submitted = true;
      }
    };
    // Change user password
    $scope.changeUserPassword = function () {
      $scope.success = $scope.error = null;
      $http.post('/users/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.passwordDetails = null;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
// Authentication service for user variables
angular.module('users').factory('Authentication', [function () {
    var _this = this;
    _this._data = { user: window.user };
    return _this._data;
  }]);'use strict';
// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', [
  '$resource',
  function ($resource) {
    return $resource('users', {}, { update: { method: 'PUT' } });
  }
]);