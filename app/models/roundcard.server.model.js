'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Roundcard Schema
 */
var RoundcardSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
     setmaxplayers:{
     type: String,
     default: '4 players'
     },

     setlocation: {
     type: String,
     required: 'location not set'
     },
     setteetime: {
     type: String,
     required: 'time not set'
     },

     setpreferredskill: {
     type: String,
     default: 'any skill',
     },

    setpreferredattitude: {
        type: String,
        default: 'casual'
    },

    roundheadline: {
        type: String,
        default: 'no headline',
        trim: true
    },
    rounddescription: {
        type: String,
        default: 'no description',
        trim: true
    }


});

mongoose.model('Roundcard', RoundcardSchema);


/*    setteeday: {
 type: Date,
 required: 'Day not chosen'
 },*/
/*USER LISTS*/
/*    setexposure: {
 type: String,
 default: 'anyone can bid',
 trim: true
 },
 player1: {
 dkusername: String,
 dkuserid: Number,
 type: Schema.ObjectId
 },
 player2: {
 dkusername: String,
 dkuserid: Number,
 type: Schema.ObjectId
 },
 player3: {
 dkusername: String,
 dkuserid: Number,
 type: Schema.ObjectId
 },
 player4: {
 dkusername: String,
 dkuserid: Number,
 type: Schema.ObjectId
 },
 *//*These arrays of strings make a living record of the handshake process *//*
 whosentbid: {
 names: Array,
 default: '',
 count: Number
 },
 whoisinvited: {
 names: Array,
 default: '',
 count: Number,
 frozen: Boolean
 },
 whoislockedin: {
 names: Array,
 default: '',
 count: Number
 },
 */