'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors'),
	Roundcard = mongoose.model('Roundcard'),
	_ = require('lodash');

/**
 * Create a Roundcard
 */
exports.create = function(req, res) {
	var roundcard = new Roundcard(req.body);
	roundcard.user = req.user;

	roundcard.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(roundcard);
		}
	});
};

/**
 * Show the current Roundcard
 */
exports.read = function(req, res) {
	res.jsonp(req.roundcard);
};

/**
 * Update a Roundcard
 */
exports.update = function(req, res) {
	var roundcard = req.roundcard ;

	roundcard = _.extend(roundcard , req.body);

	roundcard.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(roundcard);
		}
	});
};

/**
 * Delete an Roundcard
 */
exports.delete = function(req, res) {
	var roundcard = req.roundcard ;

	roundcard.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(roundcard);
		}
	});
};

/**
 * List of Roundcards
 */
exports.list = function(req, res) { Roundcard.find().sort('-created').populate('user', 'displayName').exec(function(err, roundcards) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(roundcards);
		}
	});
};

/**
 * Roundcard middleware
 */
exports.roundcardByID = function(req, res, next, id) { Roundcard.findById(id).populate('user', 'displayName').exec(function(err, roundcard) {
		if (err) return next(err);
		if (! roundcard) return next(new Error('Failed to load Roundcard ' + id));
		req.roundcard = roundcard ;
		next();
	});
};

/**
 * Roundcard authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.roundcard.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};