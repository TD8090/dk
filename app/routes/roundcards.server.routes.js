'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var roundcards = require('../../app/controllers/roundcards');

	// Roundcards Routes
	app.route('/roundcards')
		.get(roundcards.list)
		.post(users.requiresLogin, roundcards.create);

	app.route('/roundcards/:roundcardId')
		.get(roundcards.read)
		.put(users.requiresLogin, roundcards.hasAuthorization, roundcards.update)
		.delete(users.requiresLogin, roundcards.hasAuthorization, roundcards.delete);

	// Finish by binding the Roundcard middleware
	app.param('roundcardId', roundcards.roundcardByID);
};